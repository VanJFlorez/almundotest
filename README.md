This project is compiled and tested with Maven from a Linux
terminal. The edition of Java code was made with vim.
To run this project locate yourself in EjercicioJava folder.
Then, type $ mvn test

JDK 1.8.0u211
maven 3.6.1

The setup command for maven was:

mvn archetype:generate 
	-DgroupId=com.almundo.CallCenter 
	-DartifactId=EjercicioJava 
	-DarchetypeArtifactId=maven-archetype-quickstart 
	-DarchetypeVersion=1.4 
	-DinteractiveMode=false

Note that the application is concurrent and not parallel. For that
reason its used the java.util.concurrent Java's package.

The main abstractions that were made are:
	- The java application has 10 concurrent threads, each one
	  is associated with one phone line that will answer the 
	  calls.
	- A priority queue is used to handle who will answer the 
      next call. As said in consigne the highest to lowest priority,
	  is operator, supervisor, and director.

Since the priority queue will be readed and written by multiple 
threads at the same time, thread interference appears and the 
subsequent memory consistency errors. In order to treat this 
issue sinchronized methods are used and volatile idioms to guarrantee
atomic access to sensitive variables.
	

