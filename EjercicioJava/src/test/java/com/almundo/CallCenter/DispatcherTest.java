package com.almundo.CallCenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import java.util.PriorityQueue;
import java.util.List;
import java.util.LinkedList;

/**
 * Unit test for Dispatcher
 */
public class DispatcherTest {
	@Test
	public void test10Calls() {
		// For this test 13 employees are given, 8 operators, 4 supervisors
		// and 1 director to answer 10 calls. Since there are 10 calls, each 
		// operator and only two supervisors must have in its logs only one 
		// call registered, because 10 calls arrives and there are 10 phone 
		// lines (threads) available to answer them. 
		// After processing the 10 calls we must have in the dispatcher the
		// 13 employees/agents again, and each one of them with only one call
		// in their respective log, as said above.
		System.out.println("Running basic test... [13 employees, 10 calls, 10 threads ~> Quick]");
		this.testDispatcher(true, 8, 4, 1, 10, 10);

	}

	@Test
	public void testExtra1() {
		// Extra #1. Give a solution for the case when there are not idle employees.
		// Same test perfomed in the test10Calls() method, but here we provide
		// 4 employees instead of 13, that makes that the case in which all employees
		// are busy to answer entering calls. Unicity of employee's call answering
		// is not tested here, this test instance makes that employees answer more
		// than one call i.e. 4 employees and 10 calls to answer.
		System.out.println("Running Extra #1 test... [4 employees, 10 calls, 10 threads ~> Slow]");
		this.testDispatcher(false, 2, 1, 1, 10, 10);
	}

	@Test
	public void testExtra2() {
		// Extra #2. Give a solution when more than 10 calls are entering.
		// The construction of this solution handle this case easily, we generate
		// the calls randomly and store them in a queue (LinkedList callList) each 
		// time a phone line is available with a ready employee we poll that callList.
		// So in the case of more than 10 calls, that logic extends naturally.
		// Here, we formulate the same test in test10Calls(), but with 20 calls.
		// Since the employees surely will answer more than one call, we do not 
		// test unicity here.
		System.out.println("Running Extra #2 test... [13 employees, 20 calls, 10 threads ~> Quick/2]");
		this.testDispatcher(false, 8, 4, 1, 20, 10);
	}

	/**
	 * This method test the Dispatcher instances by providing the input described
	 * in the consigne.
	 * @param unicity Test if each employee must answered only one call.
	 * @param ops The number of operators
	 * @param sups The number of supervisors
	 * @param dirs The number of directors
	 * @param callQuantity The number of calls to attend
	 * @param numberOfThreads The number of concurrent threads to execute.
	 * The test is perfomed in three phases:
	 * 1. The number of employees in the priority queue after dispatcher ends its
	 *    job (i.e. answer all calls) must be equal to the initial number of 
	 *    employees (ops + sups + dirs). This test is performed because the
	 *    priority queue of employees is accessed concurrently by several threads 
	 *    and we must assure that integrity.
	 * 2. By the same reason of first phase, the number of answered calls must be
	 *    equal to the initial number of calls.
	 * 3. The sum of logged calls must be equal to the initial number of calls, and
	 *    by the construction of the test samples, each employee must answer only one
	 *    call.
	 * 4. Test the precedence in call answering by seeing the answering order of the
	 *    employees. Then all employees must have a higher position that all 
	 *    supervisors, and all supervisors must have a higher position that all 
	 *    directors.
	 */
	public void testDispatcher(boolean unicity, int ops, int sups, int dirs, int callQuantity, int numberOfThreads) {
		int counter = 0;

		List<Employee> operators	= new LinkedList<Employee>();
		List<Employee> supervisors	= new LinkedList<Employee>();
		List<Employee> directors	= new LinkedList<Employee>();
		
		Dispatcher dispatcher = new Dispatcher(ops, sups, dirs, callQuantity, numberOfThreads);
		dispatcher.startDispatcher();
		dispatcher.waitForJobDone();
		
		//  Phase #1
		//  After answering all calls, the quantity of employees must be equal.
		//  This test is performed because the employees are offer and pulled
		//  in each thread from the priority queue.
        assertTrue(ops + sups + dirs >= dispatcher.getCallAgents().size());
		
		PriorityQueue<Employee> callAgents = dispatcher.getCallAgents();	
		for(Employee e : callAgents) {  
			counter += e.getCallLogs().size();
			if(e.type == 'O')
				operators.add(e);
			else if(e.type == 'S')
				supervisors.add(e);
			else 
				directors.add(e);
		}
		//  Phase #2
		//  counter will hold the number of calls answered by the employees (or
		//  call agents), and must be equal to the quantity of calls given.
		assert(callQuantity >= counter);

		//  Phase #3	
		//  In the next couple loops is asserted that all employees must have answered
		//  only one call. For the given case in which call quantity is greater
		//  than the number of operators and lesser than the sum of operators and
		//  supervisors two loops are used. The first test that all operators must
		//  have answered only one call, the second that only (callQuantity - supervisors) supervisors
		//  must have answered one call. 		
		if(unicity) {
			for(Employee e : operators) // all ops must have one call in their logs
				assertEquals(1, e.getCallLogs().size());
			for(Employee e : supervisors)
				if(!e.getCallLogs().isEmpty()) // all sups that have logs must have only one call
					assertEquals(1, e.getCallLogs().size());
		}
		 
		//	Phase #4	
		//  These asserts proof that call answering was made in the given 
		//	precedence: ops first, sups second and dirs last, by seeing that
		//	operators must have a position answering lower that 8 (i.e. each
		//	one of them was the first 8 to answer) and that supervisors have
		//	a position greater than 8
	 	if(unicity) {
			for(Employee e : operators)
				assertTrue(e.getCallLogs().getFirst().getDepartureOrder() <= 8);
			for(Employee e : supervisors)
				if(!e.getCallLogs().isEmpty()) 
					assertTrue(e.getCallLogs().getFirst().getDepartureOrder() > 8);
		}
    }
}
