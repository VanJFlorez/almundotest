package com.almundo.CallCenter;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class PhoneLine extends Thread {
	Random rand = new Random();
	Dispatcher dispatcher;
	Lock lock = new ReentrantLock();

	/**
	 * This class construct objects that run code concurrently and
	 * have the same Dispatcher object referenced that is passed
	 * in the constuctor.
	 * @param dispatcher The common Dispatcher object shared among all threads.
	 */
	public PhoneLine(Dispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}

	public void run() {
		Double callTime;
		Employee employee;
	  	while(true) { 
			if(!dispatcher.getCallList().isEmpty()) {
				// This conditionalconditional solves extra/plus #1. If there are not more employees
				// to answer calls, the queue of random generated calls retrieved
				// by getCallList() is not polled, and the next calls waits in
				// FIFO fashion given by that queue.
				employee = dispatcher.dispatchCall();
				if(employee != null) {
					callTime = (Double) dispatcher.getCallList().poll();
					try {
						Thread.sleep(callTime.longValue()); // Call is being answered.
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					dispatcher.markCallAsDispatched();
					dispatcher.offerCallAgent(employee);
					employee.updateCallLogs(
								dispatcher.getCallsDispatched(), 
								callTime, 
								employee.getLastDepartureOrder());
				}
			} else {
				return;
			}
		}
	}
}
