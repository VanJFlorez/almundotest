package com.almundo.CallCenter;

import java.util.LinkedList;

public class Employee {
	/**
	 * char O abbreviates operator		and has priority 1
	 * char S abbreviates supervisor    and has priority 2
	 * char D abbreviates director		and has priority 3
	 */
	public char type = 'O';
	public int priority = 1;
	public int lastDepartureOrder;
	private LinkedList<CallLog> callLogs = new LinkedList<CallLog>();
	
	public Employee(char type, int priority, int lastDepartureOrder) {
		this.type = type;
		this.priority = priority;
		this.lastDepartureOrder = lastDepartureOrder;
	}

	public LinkedList<CallLog> getCallLogs() {
		return this.callLogs;
	}

	public void updateCallLogs(int position, Double callDuration, int departureOrder) {
		this.callLogs.add(new CallLog(position, callDuration, departureOrder));	
	}

	public int getLastDepartureOrder() {
		return this.lastDepartureOrder;
	}

	public int setLastDepartureOrder(int i) {
		int temp = this.lastDepartureOrder;
		this.lastDepartureOrder = i;
		return temp;
	}
}

class CallLog {
	private int position;
	private Double callDuration;
	private int departureOrder;

	/**
	 * @param pos At what position was answered this call.
	 * @param duration How much time takes this call.
	 * @param departureOrder Which position in the departure has this time this employee.
	 */
	public CallLog(int pos, Double duration, int departureOrder) {
		this.position = pos;
		this.callDuration = duration;
		this.departureOrder = departureOrder;
	}

	public double getCallDuration() {
		return this.callDuration.doubleValue();
	}

	public int getPosition() {
		return this.position;
	}

	public int getDepartureOrder() {
		return this.departureOrder;
	}
}

