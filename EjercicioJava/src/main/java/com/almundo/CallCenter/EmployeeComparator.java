package com.almundo.CallCenter;

import java.util.Comparator;

public class EmployeeComparator implements Comparator<Employee> {
	public int compare(Employee a, Employee b) {
		if (a.priority > b.priority)
			return 1;
		if (a.priority < b.priority)
			return -1;
		return 0;
	}		
}
