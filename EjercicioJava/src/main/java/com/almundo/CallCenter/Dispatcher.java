package com.almundo.CallCenter;

import java.util.PriorityQueue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Juan Camilo Flores Vanegas
 * @version 1.1
 */

public class Dispatcher {
	private int numberOfThreads;		  // The number of concurrent threads.
	private volatile int departureOrder;  // Each time that an employee departs this number is assigned.
	private volatile PriorityQueue<Employee> callAgents;     // Queue with the given employees
	private LinkedList<Double> callList;  // List that holds the call durations in millis
	private volatile int callQuantity;	  // If is not volatile, the atomic access is not guarranteed. 
	private volatile int callsDispatched;
	private boolean jobDone = false;	  // Flag that tells if the all calls are answered.
	private boolean debugMsgs = false;	  // If true, debug messages are displayed in std output.


	/**
	 * This constructor takes the argumens provided in the consigne:
	 *	@param ops The number of operators.
	 *	@param sups The number of supervisors.
	 *	@param dirs The number of directors.
	 *	@param callQuantity The number of calls to answer.
	 */
	public Dispatcher(int ops, int sups, int dirs, int callQuantity, int numberOfThreads) {
		this.numberOfThreads = numberOfThreads;
		this.departureOrder = 0;
		this.callQuantity = callQuantity;
		this.callsDispatched = 0;

		Random rand = new Random();
		this.callList = new LinkedList<Double>();
		for(int i = 0; i < callQuantity; i++) // fills the list with random call durations in milliseconds
			callList.offer(new Double((rand.nextDouble()*5 + 5)*1000));   
		
		EmployeeComparator comparator = new EmployeeComparator();	
		this.callAgents = new PriorityQueue<Employee>(comparator);
		for(int i = 0; i < ops; i++) // fill the priority queue with the given number of operators
			callAgents.offer(new Employee('O', 1, -1));
		for(int i = 0; i < sups; i++) // fill the priority queue with the given number of supervisors
			callAgents.offer(new Employee('S', 2, -1));
		for(int i = 0; i < dirs; i++) // fill the priority queue with the given number of directors 
			callAgents.offer(new Employee('D', 3, -1));
	}

	/**
	 * Starts the threads that will answer the calls for an instance
	 * of this dispatcher.
	 * @param numberOfThreads The number of concurrent threads to start.
	 */
	public void startDispatcher() {
		ArrayList<Thread> threads = new ArrayList<Thread>(); 
		for(int i = 0; i < this.numberOfThreads; i++)
			threads.add(new Thread(new PhoneLine(this)));
		for(int i = 0; i < this.numberOfThreads; i++)
			threads.get(i).start();
	}

	/**
	 * Waits until dispatcher answered all calls succesfully. This waits ends
	 * when the calls answered (getCallsDispatched()) is equal to the number
	 * of calls given (callQuantity). After that sets the jobDone flag.
	 */
	public void waitForJobDone() {
		if(debugMsgs)
			System.out.println("waiting for job done...");
		while(this.getCallsDispatched() != this.callQuantity);
		this.jobDone = true;
		if(debugMsgs)
			System.out.println("job done!");
	}

	/**
	 * Getter for private jobDone field.
	 */
	public boolean isJobDone() {
		return jobDone;
	}

	/**
	 * This standard main method is used for fast debuging puposes, 
	 * and is not used in the maven tests.
	 */
	public static void main( String[] args ) throws InterruptedException {
		Dispatcher d = new Dispatcher(10, 5, 1, 15, 14);
		for(int i = 0; i < 10; i++)
			(new Thread(new PhoneLine(d))).start();		
	}

	/**
	 * This is the method required in the consigne. Since will be called
	 * from several concurrent threads, its declared as synchronized. Each
	 * time is called, locks the current Dispatcher instance and poll an
	 * Employee from the priority queue. Note that the priority queue 
	 * abstract the idea that the operators answer first, supervisors after
	 * and directors last.
	 */
	public synchronized Employee dispatchCall() {
		if(debugMsgs)
			System.out.println(Thread.currentThread().getName() + "\tanswered a call, " +
					   						(this.callAgents.size() - 1) + " available.");
		Employee e = this.callAgents.poll();
		if(e != null) {
			departureOrder++;
			if(debugMsgs) System.out.println("Employee has departureOrder: " + departureOrder);
			e.setLastDepartureOrder(departureOrder);
			return e;
		} else {
			return null;
		}
	}

	/**
	 * This method is called in PhoneLine threaded objects, after the call
	 * ends i.e. when the .sleep() has completed the waiting time, to add 
	 * or offer the employee to the priority queue.
	 * @param employee The employee to offer to the priority queue.
	 */
	public synchronized void offerCallAgent(Employee employee) {
		if(debugMsgs)
			System.out.println(Thread.currentThread().getName() + 
									"\temployee with logs: " + 
										employee.getCallLogs() + " back to the queue, " + 
										(this.callAgents.size() + 1) + " available.");
		this.callAgents.offer(employee);
	}
	
	/**
	 * Synchronized getter for the call list.
	 */
	public synchronized LinkedList getCallList() {
		return this.callList;
	}

	/**
	 * Gets the queue of callAgents.
	 */
	public PriorityQueue getCallAgents() {
		return this.callAgents;
	}

	/**
	 * Increments by one the counter that holds the number of calls dispatched.
	 */
	public synchronized void markCallAsDispatched() {
		this.callsDispatched++;
	}

	/**
	 * If this method is not synchronized, when is called from 
	 * test module will retrieve a non-synchronized instance of the
	 * dispatcher i.e. an old version of the instance that is not
	 * updated by the concurrent threads.
	 */
	public synchronized int getCallsDispatched() {
		return this.callsDispatched;
	}
}
